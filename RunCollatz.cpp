// --------------
// RunCollatz.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Collatz.hpp"

using namespace std;

// ----
// main
// ----

int main () {
    pre_compute();
    collatz_solve(cin, cout);
    return 0;
}
