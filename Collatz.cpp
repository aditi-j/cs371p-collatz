// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;
int cache[1000000];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// cycle_length
// ------------

int cycle_length(long num) {
    assert(num > 0);
    int count = 1;
    while (num > 1) {
        assert(num > 1);
        if ((num % 2) == 1) {
            num = num + (num << 1) + 1;
            count++;
        } else {
            num /= 2;
            count++;
        }
        assert(count < 525);
    }
    assert(num <= 1);
    return count;
}

// ------------
// pre_compute
// ------------

void pre_compute() {
    for (long num = 0; num <= 100000; num++) {
        cache[num] = cycle_length(num);
    }
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    // create cache
    cache[999999] = {0};
    int maxCycle = 0;
    // find the cycle for each num btwn i and j, store max
    for (long num = min(i, j); num <= max(i, j); num++) {
        assert((num > 0) && (num < 1000000));
        int numCycle = 0;
        // if cycle is not in cache, caclculate and store cycle
        if (cache[num] == 0) {
            numCycle = cycle_length(num);
            cache[num] = numCycle;
        } else {
            numCycle = cache[num];
        }
        assert(numCycle < 525);
        if (numCycle > maxCycle) {
            maxCycle = numCycle;
        }
    }
    return make_tuple(i, j, maxCycle);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
