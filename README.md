# CS371p: Object-Oriented Programming Collatz Repo

* Name: Aditi Jain

* EID: aj29462

* GitLab ID: aditi-j

* HackerRank ID: aditiawe1312

* Git SHA: 48a6e851d88fb7d2c4af1c0ad7a0002b554d64da

* GitLab Pipelines: https://gitlab.com/aditi-j/cs371p-collatz/-/pipelines 

* Estimated completion time: 20 hours

* Actual completion time: 24 hours

* Comments: I originally thought that it would take me around 20 hours because I would run into a bug that would be difficult to fix or because I would probably get coder's block and that's exactly what happened! I'm surprised my guess was pretty accurate.